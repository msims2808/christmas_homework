const movies = [
    {"title": "Jaws", "director": "Steven Spielberg", "year": "1975"},
    {"title": "Star Wars", "director": "George Lucas", "year": "1977"},
    {"title": "Avengers: Infinity War", "director": "Anthony and Joe Russo", "year": "2018"},
    {"title": "Top Gun", "director": "Tony Scott", "year": "1986"},
    {"title": "Justice League", "director": "Zack Snyder", "year": "2017"}
];

// // Part 1 - Create a function to find a movie and output it's detail
        
//  function findMovie(movieTitle)  {
//     for (let movie of movies)  {
//         if (movie.title === movieTitle) {
//         console.log(`${movieTitle} is directed by ${movie.director} and made in ${movie.year}`);
//     }

//     console.log(movie);
//  }   

//     console.log(movie);

//  }

//  findMovie("Jaws");

//  findMovie("Star Wars");
//  console.log(movie);

// var movie = "Thor:Ragnorok"
// console.log(movie);
// // 13.this logs out Thor:Ragnorok but no longer logs out the movies variable
// // 14. Changing let to var and uncommenting all console logs
// // now logs all keys and values again

// findMovie(movie);

// // Part 2 = Create a function to return a movie object

function returnMovie(movieTitle) {
    for (let movie of movies) {
        if (movie.title === movieTitle){
    return movie
    }
    console.log(movie); 
    }

// console.log("Any text, any text at all");
return `Movie not found`;
}
// 3.	In the body of the script, declare a variable called myMovie and set it to the result of calling returnMovie with an argument of "Avengers: Infinity War".

let myMovie = returnMovie("Avengers: Infinity War");

// 4.	Log out the value of myMovie, save and observe the output.

console.log(myMovie);



// 5.	Access the properties of myMovie to produce and log a string as a sentence with them in it, saving and observing your output.

console.log(`${myMovie.title} is a film by ${myMovie.director} released in the year ${myMovie.year}`);

// What happens if we try to pass a movie title that doesn't exist in the movies array into returnMovie?  Let's find out!
// 6.	Declare a variable myOtherMovie and set its value to a call to returnMovie with an argument of "Thor: Ragnorok".

let myOtherMovie = returnMovie("Thor: Ragnorok");
console.log(myOtherMovie);

// 7.	Log out the value of myOtherMovie and observe the output.

console.log(myOtherMovie);

//The first thing that we notice is that the whole of the movies array has been logged out and the text "Any text, any text at all".
//This didn't happen = what went wrong???

function myMovieDetails(anyMovie)   {
    if (typeof(anyMovie) ==='object') {
            return (`${anyMovie.title} is a film by ${anyMovie.director} made in ${anyMovie.year}`);
    } else {
            return (anyMovie);
           }  
        }


        // 12.	Inside a console.log, call myMovieDetails with an argument of myOtherMovie.
console.log(myMovieDetails(myOtherMovie));

// 14.	Repeat the last instruction instead passing in returnMovie with an argument of "Jaws" as the argument to the myMovieDetails function.

console.log(myMovieDetails(returnMovie("Jaws")));

//wasn't working, forgot to add 'any' in line 79 before Movie.


