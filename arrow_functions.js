//see basic use of arrow function

// hello = function() {
//     return "Hello World!";
// }

// hello = () => {
//     return "Hello World!";
// }

// hello = () => "Hello World!";

let div = document.querySelector("div");

div.addEventListener("click", function(){
    console.log(this);
});

div.addEventListener("click", ()=>{
    console.log(this);
});
